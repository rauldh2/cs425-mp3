package main

import (
	"BranchServer"
	"bufio"
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatal("Usage: ./server <branch> <config file>")
	}
	branch := os.Args[1]
	configFile := os.Args[2]

	file, err := os.Open(configFile)
	if err != nil {
		log.Fatalf("Error opening config file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	serverConfigs := []BranchServer.ServerConfig{}
	var me BranchServer.ServerConfig
	for scanner.Scan() {
		parts := strings.Fields(scanner.Text())
		serverInfo := BranchServer.ServerConfig{parts[0], parts[1], parts[2]}
		serverConfigs = append(serverConfigs, serverInfo)
		if serverInfo.Branch == branch {
			me = serverInfo
		}
	}

	branchServer := BranchServer.NewBranchServer()

	branchServer.Start(me, serverConfigs)
	select {}
}
