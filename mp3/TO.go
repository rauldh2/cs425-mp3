package main

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/emirpasic/gods/maps/treemap"
)

func Contains(dict *treemap.Map, val int) bool {
	k, _ := dict.Get(val)
	return k != nil
}

type Account struct {
	ReadHistory  *treemap.Map
	WriteHistory *treemap.Map
	RCommitted   int
	WCommitted   int
	Lock         *sync.Mutex
	Cond         *sync.Cond
}

func NewAccount() *Account {
	lock := new(sync.Mutex)
	cond := sync.NewCond(lock)
	return &Account{
		ReadHistory:  treemap.NewWithIntComparator(),
		WriteHistory: treemap.NewWithIntComparator(),
		RCommitted:   0,
		WCommitted:   0,
		Lock:         lock,
		Cond:         cond,
	}
}

var accounts = map[string]*Account{}
var pending = treemap.NewWithIntComparator()
var accountsLock = sync.Mutex{}
var accountsCond = sync.NewCond(&accountsLock)

// Gets the account corresponding to the name
func GetAccount(account_name string, Tc int) *Account {
	accountsLock.Lock()
	pending.Put(Tc, -1)
	account, found := accounts[account_name]
	if !found {
		account = NewAccount()
		accounts[account_name] = account
	}
	accountsLock.Unlock()
	return account
}

// Handles a write request from the coordinator
func HandleWrite(account_name string, amount int, Tc int) error {
	account := GetAccount(account_name, Tc)
	// perform write (following pseudocode from slides)
	account.Lock.Lock()
	defer account.Lock.Unlock()
	maxReadTS := -1
	if account.ReadHistory.Size() > 0 {
		k, _ := account.ReadHistory.Max()
		maxReadTS = k.(int)
	}

	if Tc >= maxReadTS && Tc > account.WCommitted {
		account.WriteHistory.Put(Tc, amount)
	} else {
		return errors.New("Out of date write attempted")
	}
	return nil
}

// Handling a read request from the coordinator
func HandleRead(account_name string, Tc int) (int, error) {
	account := GetAccount(account_name, Tc)
	//perform read (following pseudocode from slides)
	account.Lock.Lock()
	if Tc > account.WCommitted {
		k, v := account.WriteHistory.Floor(Tc)
		if k == nil {
			account.Lock.Unlock()
			return -1, errors.New("No history on this account")
		}
		Ds_time, Ds_value := k.(int), v.(int)
		if Ds_time <= account.RCommitted { // if (Ds is committed)
			account.ReadHistory.Put(Tc, 0) // dummy value (maybe a problem?)
			account.Lock.Unlock()
			return Ds_value, nil
		} else {
			if Ds_time == Tc { // if Ds was written by Tc, simply read Ds
				account.Lock.Unlock()
				return Ds_value, nil
			} else {
				account.Cond.Wait() // can actually optimize this further: if Rcommit >= Ds or Ds not in tree
				account.Lock.Unlock()
				return HandleRead(account_name, Tc)
			}
		}
	} else {
		account.Lock.Unlock()
		return -1, errors.New("Out of date read attempted")
	}
}

// Takes a transaction out of pending
//
// Ask broadcasts is a validate cmd is waiting on pending
func Yeet(Tc int) {
	accountsLock.Lock()
	pending.Remove(Tc)
	accountsLock.Unlock()
	accountsCond.Broadcast()
	AccountApply(func(account *Account) bool { // notify any waiting read attempts
		account.Cond.Broadcast()
		return true
	})
}

// Applies a function to every account
//
// if the lambda returns false the function stops and returns false
func AccountApply(lambda func(*Account) bool) bool {
	accountsLock.Lock()
	defer accountsLock.Unlock()
	for _, account := range accounts {
		account.Lock.Lock()
		valid := lambda(account)
		if !valid {
			account.Lock.Unlock()
			return false
		}
		account.Lock.Unlock()
	}
	return true
}

// Wait for a transaction to be earliest pending or aborted
//
// Returns if still active (i.e. not aborted)
func WaitFor(Tc int) bool {
	accountsLock.Lock() // this code is a little sus
	// basically it only commits if this Tc is the earliest one
	currMin, _ := pending.Min()
	for Contains(pending, Tc) && Tc != currMin.(int) {
		accountsCond.Wait()
		currMin, _ = pending.Min()
	}
	active := Contains(pending, Tc)
	accountsLock.Unlock()
	return active
}

// Checks if a transaciton can be committed
//
// 1. Must be the earliest transaction (i.e. previous transaction are committed or arborted)
//
// 2. Must have nonnegative balances
func ValidateCommit(Tc int) bool {
	active := WaitFor(Tc)
	if !active {
		return false
	}
	valid := AccountApply(func(account *Account) bool {
		v, found := account.WriteHistory.Get(Tc)
		if found {
			return v.(int) >= 0
		} else {
			return true
		}
	})
	return valid
}

// Aborts a transaction
func HandleAbort(Tc int) {
	AccountApply(func(account *Account) bool {
		account.ReadHistory.Remove(Tc)
		account.WriteHistory.Remove(Tc)
		return true
	})
	Yeet(Tc)
}

// Commits a transaction
func HandleCommit(Tc int) {
	AccountApply(func(account *Account) bool {
		account.RCommitted = max(account.RCommitted, Tc)
		account.WCommitted = max(account.WCommitted, Tc)
		return true
	})
	Yeet(Tc)
}

func main() {
	Tc := 100
	acc1 := "gura"
	fmt.Println(HandleRead(acc1, Tc))
	fmt.Println(HandleWrite(acc1, 5, Tc))
	fmt.Println(HandleRead(acc1, Tc))
	// fmt.Println(HandleWrite(acc1, -1, Tc))
	fmt.Println(ValidateCommit(Tc))
	HandleCommit(Tc)
	fmt.Println(HandleRead(acc1, 99))
	fmt.Println(HandleWrite("kronii", 69, 101))
	go func() {
		fmt.Println("Delayed read")
		fmt.Println(HandleRead("kronii", 102))
		fmt.Println("Delayed Read success")
	}()
	go func() {
		time.Sleep(2 * time.Second)
		// HandleAbort(101)
		HandleCommit(101)
		time.Sleep(2 * time.Second)
	}()
	time.Sleep(5 * time.Second)
}
