package main

import (
	"BranchServer"
	"bufio"
	"fmt"
	"log"
	"math/rand/v2"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatal("Usage: ./client <client_id> <config file>")
	}
	_ = os.Args[1] // !! use client id for something?
	config := os.Args[2]

	configFile, err := os.Open(config)
	if err != nil {
		log.Fatalf("Error opening config file: %v", err)
	}
	defer configFile.Close()

	scanner := bufio.NewScanner(configFile)
	serverConfigs := []BranchServer.ServerConfig{}
	for scanner.Scan() {
		parts := strings.Fields(scanner.Text())
		serverInfo := BranchServer.ServerConfig{parts[0], parts[1], parts[2]}
		serverConfigs = append(serverConfigs, serverInfo)
	}
	coordinator := serverConfigs[rand.IntN(len(serverConfigs))]
	address := coordinator.Address + ":" + coordinator.Port
	// if address == "" {
	// 	log.Fatalf("No configuration found for branch %s", clientID)
	// }

	// fmt.Println("Attempting to connect to server at", clientID, address)
	var client *rpc.Client
	for {
		client, err = rpc.DialHTTP("tcp", address)
		if err == nil {
			break
		}
		time.Sleep(5 * time.Millisecond)
	}

	// fmt.Println("DialHTTP Successful, connected to initial server")
	Tc := time.Now().UnixNano()
	// fmt.Println("I'm", Tc)
	inputScanner := bufio.NewScanner(os.Stdin)
	for inputScanner.Scan() {
		command := strings.Fields(inputScanner.Text())
		if len(command) == 0 {
			continue
		}

		switch command[0] {
		case "BEGIN":
			fmt.Println("OK")
		case "DEPOSIT":
			query := strings.Split(command[1], ".")
			branch, account := query[0], query[1]
			amount, _ := strconv.Atoi(command[2])
			args := BranchServer.DepositQuery{Branch: branch, Account: account, Amount: int64(amount), Tc: int64(Tc)}
			var dummy int
			err = client.Call("BranchServer.Deposit", &args, &dummy)
			if err != nil {
				args := int64(Tc)
				client.Call("BranchServer.Abort", &args, &dummy)
				fmt.Println(err)
				return
			} else {
				fmt.Println("OK")
			}
		case "WITHDRAW":
			query := strings.Split(command[1], ".")
			branch, account := query[0], query[1]
			amount, _ := strconv.Atoi(command[2])
			args := BranchServer.WithdrawQuery{Branch: branch, Account: account, Amount: int64(amount), Tc: int64(Tc)}
			var dummy int
			err = client.Call("BranchServer.Withdraw", &args, &dummy)
			if err != nil {
				args := int64(Tc)
				client.Call("BranchServer.Abort", &args, &dummy)
				fmt.Println(err)
				return
			} else {
				fmt.Println("OK")
			}
		case "BALANCE":
			query := strings.Split(command[1], ".")
			branch, account := query[0], query[1]
			args := BranchServer.BalanceQuery{Branch: branch, Account: account, Tc: int64(Tc)}
			var result int64
			err = client.Call("BranchServer.Balance", &args, &result)
			if err != nil {
				args := int64(Tc)
				client.Call("BranchServer.Abort", &args, &result)
				fmt.Println(err)
				return
			} else {
				fmt.Printf("%s = %d\n", command[1], result)
			}
		case "COMMIT":
			args := int64(Tc)
			var dummy int
			err := client.Call("BranchServer.Commit", &args, &dummy)
			if err == nil {
				fmt.Println("COMMIT OK")
			} else {
				fmt.Println("ABORTED")
			}
			return
		case "ABORT":
			var dummy int
			args := int64(Tc)
			client.Call("BranchServer.Abort", &args, &dummy)	
			fmt.Println("ABORTED")
			return
		default:
			fmt.Println("Unknown command")
		}
	}

	client.Close()
}
