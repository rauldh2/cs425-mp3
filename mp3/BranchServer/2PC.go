package BranchServer

import (
	"errors"
)

// Applies a function to every account
// if the lambda returns false the function stops and returns false
func (bs *BranchServer) AccountApply(lambda func(*Account) bool) bool {
	bs.accountsLock.Lock()
	defer bs.accountsLock.Unlock()
	for _, account := range bs.accounts { //!! do you need a lock here?
		account.Lock.Lock()
		valid := lambda(account)
		if !valid {
			account.Lock.Unlock()
			return false
		}
		account.Lock.Unlock()
	}
	return true
}

// Takes a transaction out of pending
//
// Ask broadcasts is a validate cmd is waiting on pending
func (bs *BranchServer) Discard(Tc int64) {
	// fmt.Println("Discarding", Tc)
	bs.accountsLock.Lock()
	bs.pending.Remove(int(Tc))
	bs.accountsLock.Unlock()
	bs.accountsCond.Broadcast()
	bs.AccountApply(func(account *Account) bool { // notify any waiting read attempts
		// fmt.Println("Waking up any waiting boys")
		account.Cond.Broadcast()
		return true
	})
}

// Wait for a transaction to be earliest pending or aborted
//
// Returns if still active (i.e. not aborted)
func (bs *BranchServer) WaitFor(Tc int) bool {
	bs.accountsLock.Lock() // this code is a little sus
	// basically it only commits if this Tc is the earliest one
	readyChecker := func(acc *Account) bool {
		ready := true
		// if Contains(acc.ReadHistory, Tc) {
		// 	k, _ := acc.ReadHistory.Floor(Tc - 1)
		// 	if k != nil {
		// 		ready = ready && !Contains(bs.pending, k.(int))
		// 	}
		// }
		if Contains(acc.WriteHistory, Tc) {
			k, _ := acc.WriteHistory.Floor(Tc - 1)
			if k != nil {
				if Contains(bs.pending, k.(int)) {
					// fmt.Println(" (ready checker) Waiting on ", k, " I'm", Tc)
				}
				ready = ready && !Contains(bs.pending, k.(int))
			}
		}
		return ready
	}
	bs.accountsLock.Unlock()
	ready := bs.AccountApply(readyChecker)
	bs.accountsLock.Lock()

	for !ready {
		bs.accountsCond.Wait()
		bs.accountsLock.Unlock()
		ready = bs.AccountApply(readyChecker)
		bs.accountsLock.Lock()
	}
	// fmt.Println("Ready")
	active := Contains(bs.pending, Tc)
	bs.accountsLock.Unlock()
	return active
}

// Checks if a transaciton can be committed
//
// 1. Must be the earliest transaction (i.e. previous transaction are committed or arborted)
//
// 2. Must have nonnegative balances
func (bs *BranchServer) ValidateCommit(Tc *int64, reply *int64) error {
	// fmt.Println("Trying to validate", *Tc)
	_ = bs.WaitFor(int(*Tc))
	// fmt.Println("Done")
	// if !active {
	// 	fmt.Println("Already dealt")
	// 	return errors.New("already read")
	// }
	valid := bs.AccountApply(func(account *Account) bool {
		v, found := account.WriteHistory.Get(int(*Tc))
		if found {
			return v.(int64) >= 0
		} else {
			return true
		}
	})
	if !valid {
		// fmt.Println("I do not approve")
		// fmt.Println(*bs)
		return errors.New("ABORTED")
	} else {
		return nil
	}
}

// Aborts a transaction
func (bs *BranchServer) HandleAbort(Tc *int64, reply *int64) error {
	bs.AccountApply(func(account *Account) bool {
		account.ReadHistory.Remove(int(*Tc))
		account.WriteHistory.Remove(int(*Tc))
		return true
	})
	bs.Discard(*Tc)
	return nil
}

func (bs *BranchServer) HandleCommit(Tc *int64, reply *int64) error {
	bs.AccountApply(func(account *Account) bool {
		_, found := account.ReadHistory.Get(int(*Tc))
		if found {
			account.RCommitted = max(account.RCommitted, *Tc)
		}
		_, found = account.WriteHistory.Get(int(*Tc))
		if found {
			account.WCommitted = max(account.WCommitted, *Tc)
		}
		return true
		// account.RCommitted = max(account.RCommitted, *Tc)
		// account.WCommitted = max(account.WCommitted, *Tc)
		return true
	})
	bs.Discard(*Tc)
	return nil
}
