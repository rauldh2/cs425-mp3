package BranchServer

import (
	"errors"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"sync"

	"github.com/emirpasic/gods/maps/treemap"
)

func Contains(dict *treemap.Map, val int) bool {
	k, _ := dict.Get(int(val))
	return k != nil
}

type Account struct {
	ReadHistory  *treemap.Map
	WriteHistory *treemap.Map
	RCommitted   int64
	WCommitted   int64
	Lock         *sync.Mutex
	Cond         *sync.Cond
}

func NewAccount() *Account {
	lock := new(sync.Mutex)
	cond := sync.NewCond(lock)
	return &Account{
		ReadHistory:  treemap.NewWithIntComparator(),
		WriteHistory: treemap.NewWithIntComparator(),
		RCommitted:   0,
		WCommitted:   0,
		Lock:         lock,
		Cond:         cond,
	}
}

type BranchServer struct {
	accounts     map[string]*Account
	pending      *treemap.Map
	accountsLock *sync.Mutex
	accountsCond sync.Cond
	wg           sync.WaitGroup
	servers      sync.Map
}

// Gets the account corresponding to the name
func (bs *BranchServer) GetAccount(account_name string, Tc int64) *Account {
	bs.accountsLock.Lock()
	bs.pending.Put(int(Tc), -1)
	account, found := bs.accounts[account_name]
	if !found {
		account = NewAccount()
		account.ReadHistory.Put(int(Tc), int64(0))
		bs.accounts[account_name] = account
	}
	bs.accountsLock.Unlock()
	return account
}

type BSWriteArgs struct {
	AccountName string
	Amount      int64
	Tc          int64
}

// Handles a write request from the coordinator
func (bs *BranchServer) HandleWrite(args *BSWriteArgs, reply *int) error {
	account_name := args.AccountName
	Tc := args.Tc
	amount := args.Amount
	account := bs.GetAccount(account_name, Tc)
	// perform write (following pseudocode from slides)
	account.Lock.Lock()
	defer account.Lock.Unlock()
	maxReadTS := -1
	if account.ReadHistory.Size() > 0 {
		k, _ := account.ReadHistory.Max()
		maxReadTS = k.(int)
	}

	if int(Tc) >= maxReadTS && Tc > account.WCommitted {
		account.WriteHistory.Put(int(Tc), amount)
	} else {
		return errors.New("ABORTED")
	}
	return nil
}

type BSReadArgs struct {
	AccountName string
	Tc          int64
}

// Handling a read request from the coordinator
func (bs *BranchServer) HandleRead(args *BSReadArgs, reply *int) error {
	account_name := args.AccountName
	Tc := args.Tc
	account := bs.GetAccount(account_name, Tc)
	//perform read (following pseudocode from slides)
	account.Lock.Lock()
	if Tc > account.WCommitted {
		k, v := account.WriteHistory.Floor(int(Tc))
		if k == nil {
			account.Lock.Unlock()
			return errors.New("NOT FOUND, ABORTED")
		}
		Ds_time, Ds_value := k.(int), v.(int64)

		if Ds_time <= int(account.WCommitted) { // if (Ds is committed)
			account.ReadHistory.Put(int(Tc), 0) // dummy value (maybe a problem?)
			account.Lock.Unlock()
			*reply = int(Ds_value)
			return nil
		} else {
			if Ds_time == int(Tc) { // if Ds was written by Tc, simply read Ds
				account.Lock.Unlock()
				*reply = int(Ds_value)
				return nil
			} else {
				// fmt.Println("(read rule) Waiting for ", Ds_time, " I'm", Tc)
				account.Cond.Wait() // can actually optimize this further: if Rcommit >= Ds or Ds not in tree
				account.Lock.Unlock()
				return bs.HandleRead(args, reply)
			}
		}
	} else {
		account.Lock.Unlock()
		// fmt.Println(*bs)
		// fmt.Println(Tc)
		return errors.New("ABORTED")
	}
}

func NewBranchServer() *BranchServer {
	lock := sync.Mutex{}
	bs := BranchServer{
		accounts:     map[string]*Account{},
		pending:      treemap.NewWithIntComparator(),
		accountsLock: &lock,
		accountsCond: sync.Cond{L: &lock},
	}
	return &bs
}

func (bs *BranchServer) Start(Me ServerConfig, peers []ServerConfig) { // this list include self
	rpc.Register(bs)
	rpc.HandleHTTP()
	l, err := net.Listen("tcp", Me.Address+":"+Me.Port)
	if err != nil {
		log.Fatal("listen error: ", err)
	}
	bs.wg.Add(len(peers))
	go http.Serve(l, nil)
	go bs.Connect(peers)
}
