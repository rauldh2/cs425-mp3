package BranchServer

import (
	"errors"
	"net/rpc"
	"time"
)

type BalanceQuery struct {
	Branch  string
	Account string
	Tc      int64
}

func (c *BranchServer) Balance(args *BalanceQuery, reply *int64) error {
	c.wg.Wait()
	server, _ := c.servers.Load(args.Branch)
	serverArgs := BSReadArgs{AccountName: args.Account, Tc: args.Tc}
	var balance int64
	err := server.(*rpc.Client).Call("BranchServer.HandleRead", &serverArgs, &balance)
	if err != nil {
		return err
	}
	*reply = balance
	return nil
}

type WithdrawQuery struct {
	Branch  string
	Account string
	Amount  int64
	Tc      int64
}

func (c *BranchServer) Withdraw(args *WithdrawQuery, reply *int) error {
	c.wg.Wait()
	server, _ := c.servers.Load(args.Branch)
	readArgs := BSReadArgs{AccountName: args.Account, Tc: args.Tc}
	var balance int64
	err := server.(*rpc.Client).Call("BranchServer.HandleRead", &readArgs, &balance)
	if err != nil {
		return err
	}
	new_balance := balance - args.Amount
	writeArgs := BSWriteArgs{AccountName: args.Account, Amount: new_balance, Tc: args.Tc}
	var dummy int64
	err = server.(*rpc.Client).Call("BranchServer.HandleWrite", &writeArgs, &dummy)
	if err != nil {
		return err
	}
	return nil
}

type DepositQuery struct {
	Branch  string
	Account string
	Amount  int64
	Tc      int64
}

func (c *BranchServer) Deposit(args *DepositQuery, reply *int) error {
	c.wg.Wait()
	server, _ := c.servers.Load(args.Branch)
	readArgs := BSReadArgs{AccountName: args.Account, Tc: args.Tc}
	var balance int64

	err := server.(*rpc.Client).Call("BranchServer.HandleRead", &readArgs, &balance)
	if err != nil { // account doesn't exist (so create it with this bad boy)
		balance = 0
	}
	new_balance := balance + args.Amount

	writeArgs := BSWriteArgs{AccountName: args.Account, Amount: new_balance, Tc: args.Tc}
	var dummy int64
	err = server.(*rpc.Client).Call("BranchServer.HandleWrite", &writeArgs, &dummy)
	if err != nil {
		return err
	}
	return nil
}

func (c *BranchServer) Commit(Tc *int64, reply *int) error {
	success := true
	c.servers.Range(func(branch any, server any) bool {
		var dummy int
		err := server.(*rpc.Client).Call("BranchServer.ValidateCommit", Tc, &dummy)
		if err != nil {
			success = false
			return false
		}
		return true
	})
	// fmt.Println("Sucess: ", success)
	c.servers.Range(func(branch any, server any) bool {
		var dummy int
		if success {
			_ = server.(*rpc.Client).Call("BranchServer.HandleCommit", Tc, &dummy)
		} else {
			_ = server.(*rpc.Client).Call("BranchServer.HandleAbort", Tc, &dummy)
		}
		return true
	})
	if !success {
		return errors.New("ABORTED")
	}
	return nil
}

func (c *BranchServer) Abort(Tc *int64, reply *int) error {
	c.servers.Range(func(branch any, server any) bool {
		var dummy int
		_ = server.(*rpc.Client).Call("BranchServer.HandleAbort", Tc, &dummy)
		return true
	})
	return nil
}

type ServerConfig struct {
	Branch  string
	Address string
	Port    string
}

func (c *BranchServer) Connect(server_addresses []ServerConfig) {
	for _, serverConfig := range server_addresses {
		for {
			client, err := rpc.DialHTTP("tcp", serverConfig.Address+":"+serverConfig.Port)
			if err == nil {
				c.servers.Store(serverConfig.Branch, client)
				c.wg.Done()
				break
			}
			time.Sleep(5 * time.Millisecond)
		}
	}
}
